package MethoddanData.StudiKasus.Mahasiswa;

public class Mahasiswa {
    private String nama;
    private String nim;
    private String kelompok;
    private String alamat;
    private String hoby;

    public void setNama(String nama) {
        this.nama = nama;
    }
    public void setNim(String nim) {
        this.nim = nim;
    }
    public void setKelompok(String kelompok) {
        this.kelompok = kelompok;
    }
    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
    public void setHoby(String hoby) {
        this.hoby = hoby;
    }
    public String getNama() {
        return nama;
    }
    public String getNim() {
        return nim;
    }
    public String getKelompok() {
        return kelompok;
    }
    public String getAlamat() {
        return alamat;
    }
    public String getHoby() {
        return hoby;
    }


    public String cetakNama(String x){
        String cetak = "Nama : " + x;
        return  cetak;
    }
    public String cetakNim(String y){

        String cetak = "NIM : " + y;
        return  cetak;
    }
    public String cetakKel(String z){
        String cetak = "Kelompok " + z;
        return  cetak;
    }
    public String cetakAlamat(String a){

        String cetak = "Alamat : " + a;
        return  cetak;
    }
    public String cetakHoby(String b){

        String cetak = "Hoby : " + b;
        return  cetak;
    }

    public void display(){
        System.out.println("\n ==== Ini adalah biodata mahasiswa " + this.nama.toUpperCase() + "===");
    }
}
