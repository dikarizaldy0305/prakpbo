package MethoddanData.StudiKasus;

class Olahraga{
    String nama;
    String asal;
    String cabangOlahraga;

    public Olahraga(){
        this.nama = "M. Zohri";
        this.asal = "Indonesia";
        this.cabangOlahraga = "100 Meter Putra";
    }

    public Olahraga(String nama){
        this.nama = nama;
    }
    public Olahraga(String nama, String asal){
        this.nama = nama;
        this.asal = asal;
    }
    public Olahraga(String nama, String asal, String cabangOlahraga){
        this.nama = nama;
        this.asal = asal;
        this.cabangOlahraga = cabangOlahraga;
    }
    void display(){
        System.out.println("Nama            : "+nama);
        System.out.println("Asal            : "+asal);
        System.out.println("Cabang Olahraga : "+cabangOlahraga);
        System.out.println("---------------------------------");
    }
}
public class SK_No3 {
    public static void main(String[] args) {
        Olahraga Olga1 = new Olahraga();
        Olahraga Olga2 = new Olahraga("Jonathan Cristie");
        Olahraga Olga3 = new Olahraga("Ezra Walian","Indonesia");
        Olahraga Olga4 = new Olahraga("Elkan Baggot", "Indonesia", "Sepak Bola");

        Olga1.display();
        Olga2.display();
        Olga3.display();
        Olga4.display();
        }
    }
