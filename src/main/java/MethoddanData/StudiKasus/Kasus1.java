package MethoddanData.StudiKasus;

import MethoddanData.StudiKasus.Mahasiswa.Mahasiswa;

public class Kasus1 {
    public static void main(String[] args) {
        Mahasiswa feldy = new Mahasiswa();
        Mahasiswa andika = new Mahasiswa();
        Mahasiswa tuti = new  Mahasiswa();

        //// mahasiswa feldy
        feldy.setNama("Feldy Novanda Mulkan");
        feldy.setNim("F1B019055");
        feldy.setKelompok("1");
        feldy.setAlamat("Kuripan");
        feldy.setHoby("Bermain bola");

        feldy.display();
        System.out.println(feldy.cetakNama(feldy.getNama()));
        System.out.println(feldy.cetakNim(feldy.getNim()));
        System.out.println(feldy.cetakKel(feldy.getKelompok()));
        System.out.println(feldy.cetakAlamat(feldy.getAlamat()));
        System.out.println(feldy.cetakHoby(feldy.getHoby()));

        //// mahasiswa andika
        andika.setNama("Andika Rizaldy");
        andika.setNim("F1B019024");
        andika.setKelompok("1");
        andika.setAlamat("Narmada");
        andika.setHoby("Bermain basket");

        andika.display();
        System.out.println(andika.cetakNama(andika.getNama()));
        System.out.println(andika.cetakNim(andika.getNim()));
        System.out.println(andika.cetakKel(andika.getKelompok()));
        System.out.println(andika.cetakAlamat(andika.getAlamat()));
        System.out.println(andika.cetakHoby(andika.getHoby()));

        ////mahasiswa sri ayu astuti
        tuti.setNama("Sri Astuti Karyawati");
        tuti.setNim("F1B019133");
        tuti.setKelompok("1");
        tuti.setAlamat("Praya");
        tuti.setHoby("Memasak");

        tuti.display();
        System.out.println(tuti.cetakNama(tuti.getNama()));
        System.out.println(tuti.cetakNim(tuti.getNim()));
        System.out.println(tuti.cetakKel(tuti.getKelompok()));
        System.out.println(tuti.cetakAlamat(tuti.getAlamat()));
        System.out.println(tuti.cetakHoby(tuti.getHoby()));
    }
}

