package MethoddanData.StudiKasus;

public class Mobil {
    String warna, merk;
    public Mobil() {
        warna = "Merah";
        merk  = "BMW";
    }
    public Mobil (String Warna1, String Merk1) {
        warna = Warna1;
        merk  = Merk1;
    }

    void maju() {
        System.out.println("Mobil " + merk + " warna " + warna + " bergerak maju");
    }

    void mundur() {
        System.out.println("Mobil " + merk + " warna " + warna + " bergerak mundur");
    }


    public static void main(String[] args) {
        // Membuat object mobilSaya
        Mobil mobilSaya1 = new Mobil();
        Mobil mobilSaya2 = new Mobil("Hitam","Ferrari");

        // Memanggil object
        mobilSaya1.maju();
        mobilSaya1.mundur();
        mobilSaya2.maju();
        mobilSaya2.mundur();
    }
}