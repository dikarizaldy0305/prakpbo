package MethoddanData.Jobsheet.No_7.Demo;
import MethoddanData.Jobsheet.No_7.KonversiUang.KonversiUang;

import java.util.Scanner;

public class No7 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        KonversiUang uang = new KonversiUang();
        System.out.println("-------Rupiah ke Dollar--------");
        System.out.print("Masukkan jumlah uang : ");
        double nilai = input.nextDouble();
        System.out.println("Hasil konversi adalah "+uang.hasilRupiah(nilai));
    }
}
