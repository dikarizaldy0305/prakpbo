package MethoddanData.Jobsheet.No_2;

public class No_2 {
    String[] Karnivor = {"Harimau","Singa", "Burung"};
    String[] Herbivor = {"Kelinci", "Gajah", "Jerapah"};
    void karnivora(){
        for(int x=0;x<=2;x++)
        System.out.println(Karnivor[x]);
    }
    void herbivora(){
        for(int x=0;x<=2;x++)
        System.out.println(Herbivor[x]);
    }
    public static void main(String[] args) {
        No_2 panggil2 = new No_2();
        System.out.println("Hewan Karnivora");
        panggil2.karnivora();
        System.out.println("\nHewan Herbivora");
        panggil2.herbivora();
    }
}
