package MethoddanData.Jobsheet.No_4;
import java.util.Scanner;

class Identitas{
    String nama;
    String umur;
    String alamat;
    String hobi;
}
public class No4{
    public static void main(String[] args) {
        Identitas identitas = new Identitas();
        Scanner input = new Scanner(System.in);
        System.out.print("Nama : ");
        identitas.nama = input.nextLine();
        System.out.print("Umur : ");
        identitas.umur = input.nextLine();
        System.out.print("Alamat : ");
        identitas.alamat = input.nextLine();
        System.out.print("Hobi : ");
        identitas.hobi = input.nextLine();
        input.close();

        System.out.println("\n---------- Hasil manipulasi-----------");
        System.out.println("Nama : "+identitas.nama.toUpperCase());
        System.out.println("Umur : "+identitas.umur.toUpperCase());
        System.out.println("Alamat : "+identitas.alamat.toUpperCase());
        System.out.println("Hobi : "+identitas.hobi.toUpperCase());

    }
}



