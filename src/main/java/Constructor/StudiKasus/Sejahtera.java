package Constructor.StudiKasus;

import java.util.Scanner;

public class Sejahtera {
    public static Karyawan newKaryawan[] = new Karyawan[10];
    public static int counter = 0;
    public static void main(String[] args) {
        while (true) {
            Karyawan pegawai = new Karyawan();
            Scanner input = new Scanner(System.in);
            int pilih;
            String tempUsername;
            int tempPass;
            int login =0;
            int valid = 0;
            System.out.println("1. Tambah akun");
            System.out.println("2. Login");
            System.out.println("3. Exit");
            System.out.print("Pilih : ");
            pilih = input.nextInt();

            if (pilih == 1) {
                System.out.println("\n[1] Create Account");
                System.out.print("Username : ");
                pegawai.user = input.next();
                System.out.print("Password : ");

                do{
                    pegawai.password = input.nextInt();
                    System.out.print("Verify : ");
                    pegawai.verify = input.nextInt();
                    pegawai.bool = false;
                    if (pegawai.password != pegawai.verify) {
                        pegawai.bool = false;
                        System.out.println("Masukkan password dengan benar : ");
                        continue;
                    }
                    if (pegawai.password == pegawai.verify) {
                        pegawai.bool = true;
                        System.out.println("Password sudah benar! ");
                        continue;
                    }
                }while(pegawai.bool==false);
                System.out.println("\nSelamat!");
                System.out.println("Akun anda sudah di buat!");
                newKaryawan[counter] = new Karyawan(pegawai.user, pegawai.password,pegawai.bool,pegawai.verify);
                counter++;
            } else if (pilih == 2) {
                System.out.println("[2] Login");
                do{
                    valid = 0;
                    System.out.print("Username: ");
                    tempUsername = input.next();
                    System.out.print("Password: ");
                    tempPass = input.nextInt();
                    for(int index=0; index<counter; index++){
                        if(tempUsername.equals(newKaryawan[index].user)){
                            if(tempPass == newKaryawan[index].password)
                            {
                                valid = 1;
                                login = index;
                                System.out.println("Selamat anda berhasil login ");
                                System.out.print("User : " + newKaryawan[index].user + "\n");
                            }
                        }}

                    if(valid==0){
                        System.out.println("Username or password does not exist. Please try again!");
                    }
                }while(valid!=1);

            } else if(pilih == 3){
                System.exit(0);
            }else {
                System.out.println("System Error");
                System.exit(0);
            }
        }
    }
}
class Karyawan{
    String user;
    int password;
    boolean bool;
    int verify;


    public Karyawan(){
        user = "admin";
        password = 123;
        bool = true;
        verify = 123;
    }
    public Karyawan(String user, int password, boolean bool, int verify){
        this.user = user;
        this.password = password;
        this.bool = bool;
        this.verify = verify;
    }
}

