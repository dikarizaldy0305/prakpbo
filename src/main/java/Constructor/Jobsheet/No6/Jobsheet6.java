package Constructor.Jobsheet.No6;

public class Jobsheet6 {
    String nama;
    String nim;
    Double kelompok;
    Integer tanggal;
    Jobsheet6(){
        nim = "F1B019024";
    }
    Jobsheet6(String nama){
        this.nama = nama;
    }
    Jobsheet6(Double klp){
        kelompok = klp;
    }
    Jobsheet6(Integer tgl){
        this.tanggal = tgl;
    }
    public static void main(String[] args) {
        Jobsheet6 J61 = new Jobsheet6();
        Jobsheet6 J62 = new Jobsheet6("Andika Rizaldy");
        Jobsheet6 J63 = new Jobsheet6(3);
        Jobsheet6 J64 = new Jobsheet6(1.0);
        consolelog("Nama           : " + J62.nama);
        consolelog("NIM            : " + J61.nim);
        consolelog("Kelompok       : " + J64.kelompok);
        consolelog("Tanggal Lahir  : " + J63.tanggal);
        Jobsheet6 J65 = new Jobsheet6("Kucing");
        Jobsheet6 J66 = new Jobsheet6(5);
        consolelog("Nama Hewan kesayangan : " + J65.nama);
        consolelog("NIM                   : " + J61.nim);
        consolelog("Kelompok              : " + J64.kelompok);
        consolelog("Bulan Lahir           : " + J66.tanggal);
    }
    static void consolelog(String x){
        System.out.println(x);
    }
}

