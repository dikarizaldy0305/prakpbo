package Constructor.Jobsheet.No7;

public class Jobsheet7 {
        Integer p;
        Integer l;
        Integer t;
        Integer hasil;
        Double hasil2;
        Double pjg, lbr, tg;

    Jobsheet7() {
            p = 2;
            l = 3;
            t = 4;
        }

    Jobsheet7(Integer panjang, Integer lebar, Integer tinggi){
            p = panjang;
            l = lebar;
            t = tinggi;
            hasil = p * l * t;
        }

    Jobsheet7(Double panjang, Double lebar, Double tinggi){
            pjg = panjang;
            lbr = lebar;
            tg = tinggi;
            hasil2 = pjg * lbr * tg;
        }

    Integer hitung () {
            hasil = p * l * t;
            return hasil;
        }

public static void main (String[]args){
    Jobsheet7 J7 = new Jobsheet7();
            System.out.println("Hasil perhitungan objek 1 : " + J7.hitung());
    Jobsheet7 J71 = new Jobsheet7(6, 7, 8);
            System.out.println("Hasil Perhitungan objek 2 : " +
                    J71.hasil);
    Jobsheet7 J72 = new Jobsheet7(3.2, 6.3, 9.4);
            System.out.println("Hasil Perhitungan objek 2 : " +
                    J72.hasil2);
        }
}