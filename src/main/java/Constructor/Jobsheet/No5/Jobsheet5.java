package Constructor.Jobsheet.No5;

public class Jobsheet5 {
        String nama;
        String nim;
        Double kelompok;
        Integer tanggal;
        Jobsheet5(){
            nim = "F1B019024";
        }
        Jobsheet5(String nama){
            this.nama = nama;
        }
        Jobsheet5(Double klp){
            kelompok = klp;
        }
        Jobsheet5(Integer tgl){
            this.tanggal = tgl;
        }
    public static void main(String[] args) {
        Jobsheet5 J51 = new Jobsheet5();
        Jobsheet5 J52 = new Jobsheet5("Andika Rizaldy");
        Jobsheet5 J53 = new Jobsheet5(3);
        Jobsheet5 J54 = new Jobsheet5(1.0);
        consolelog("Nama           : " + J52.nama);
        consolelog("NIM            : " + J51.nim);
        consolelog("Kelompok       : " + J54.kelompok);
        consolelog("Tanggal Lahir  : " + J53.tanggal);
        Jobsheet5 J55 = new Jobsheet5("Book");
        Jobsheet5 J56 = new Jobsheet5(5);
        consolelog("Nama Benda kesayangan : " + J55.nama);
        consolelog("NIM                   : " + J51.nim);
        consolelog("Kelompok              : " + J54.kelompok);
        consolelog("Bulan Lahir           : " + J56.tanggal);
    }
    static void consolelog(String x){
        System.out.println(x);
    }
}

