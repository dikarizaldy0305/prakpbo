package Constructor.Jobsheet.No8;

public class Jobsheet8 {
    double phi;
    double jari_jari;
    double selimut;
    double hasil;
    Jobsheet8(){
        phi = 3.14;
    }
    Jobsheet8(double r, double s){
        phi = 3.14;
        hasil = phi * r * (r + s);
    }
    Jobsheet8(double r){
        jari_jari = r;
    }
    Jobsheet8( String x, double s){
        System.out.println(x + s);
    }
    public void setJari_jari(double jari_jari) {
        this.jari_jari = jari_jari;
    }
    public void setSelimut(double selimut) {
        this.selimut = selimut;
    }
    public double getJari_jari() {
        return jari_jari;
    }
    public double getSelimut() {
        return selimut;
    }
    double hitung(double r, double s){
        hasil = phi * r * (r + s);
        return hasil;
    }

    public static void main (String[]args){
        Jobsheet8 J8 = new Jobsheet8();
            System.out.println("Nilai phi kerucut           : " + J8.phi);
        Jobsheet8 J81 = new Jobsheet8(9,1);
            System.out.println("Nilai perhitungan kerucut2  : " + J81.hasil);
        Jobsheet8 J82 = new Jobsheet8();
            J82.setJari_jari(3);
            J82.setSelimut(5);
        double hasil = J8.hitung(J82.getJari_jari(),
                J82.getSelimut());
            System.out.println("Hasil kerucut 3             : " + hasil);
        Jobsheet8 J83 = new Jobsheet8(3);
            System.out.println("Print jari kerucut4         : "+
                J83.jari_jari);
        Jobsheet8 J84 = new Jobsheet8("Selimut kerucut5            : ", 3);

    }
}