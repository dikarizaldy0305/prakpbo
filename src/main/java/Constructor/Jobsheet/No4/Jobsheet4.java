package Constructor.Jobsheet.No4;

public class Jobsheet4 {
        Integer umur;
        String angkatan;
        Integer tanggal;
        public Jobsheet4(){
            this.umur = 20;
        }
        public Jobsheet4(String angkatan){
            this.angkatan = angkatan;
        }
        public Jobsheet4(Integer tanggal){
            this.tanggal = tanggal;
        }

    public static void main(String[] args) {
        Jobsheet4 J4 = new Jobsheet4();
        Jobsheet4 J41= new Jobsheet4("2019");
        Jobsheet4 J42 = new Jobsheet4(18);
        System.out.println("Umur     : " + J4.umur);
        System.out.println("Angkatan : " +
                J41.angkatan);
        System.out.println("Tanggal  : " +
                J42.tanggal);
    }
}