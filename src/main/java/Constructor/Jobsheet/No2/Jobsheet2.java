package Constructor.Jobsheet.No2;

public class Jobsheet2 {
    public Integer angkatan;
    public Integer semester;
    public Integer umur;

    public Jobsheet2(Integer angkatan, Integer semester, Integer umur ){
        this.angkatan = angkatan;
        this.semester = semester;
        this.umur = umur;
    }

    public static void main(String[] args) {
        Jobsheet2 J2 = new Jobsheet2(2019,6,20);
        System.out.println("Angkatan : "+ J2.angkatan);
        System.out.println("Semester : "+ J2.semester);
        System.out.println("Umur     : "+ J2.umur + " Tahun");
    }
}
