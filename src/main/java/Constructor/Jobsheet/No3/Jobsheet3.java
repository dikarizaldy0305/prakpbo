package Constructor.Jobsheet.No3;

public class Jobsheet3 {
    String nama;
    String nim;
    Integer kelompok;

    public Jobsheet3(String nama){
        this.nama = nama;
    }
    public Jobsheet3(String nim, Integer kelompok ){
        this.nim = nim;
        this.kelompok = kelompok;
    }

    public static void main(String[] args) {
        Jobsheet3 J3 = new Jobsheet3("Andika Rizaldy");
        Jobsheet3 J31 = new Jobsheet3("F1B019024",1);
        System.out.println("Nama        : "+ J3.nama);
        System.out.println("NIM         : "+ J31.nim);
        System.out.println("Kelompok    : "+ J31.kelompok);
    }
}
