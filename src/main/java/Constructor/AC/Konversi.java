package Constructor.AC;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Scanner;
public class Konversi {
    public static void main(String[] args) {
        Uang uang = new Uang();
        NumberFormat format = new DecimalFormat("#0.00");
        double money, money2;
        Scanner scanner = new Scanner(System.in);
        System.out.print("==== Program Konversi Uang USD to RP====\n");
        System.out.print("Nilai Dollar saat ini : "+uang.getNilai() +" USD = Rp. " + uang.getHasil());
        System.out.print("\nMasukkan uang (dalam dollar) : USD. ");
        money= scanner.nextDouble();
        Uang uang2 = new Uang(money);
        System.out.println("\nHasil konversi (dollar statis) : Rp. "+ uang2.getHasil());
        System.out.println("==== nilai uang dan dollar Dinamis ===");
        System.out.print("Masukkan uang (dalam dollar) : USD. ");
        money = scanner.nextDouble();
        System.out.print("\nMasukkan nilai dollar (rupiah) : Rp. ");
        money2 = scanner.nextDouble();
        Uang uang3 = new Uang(money, money2);
        System.out.println("Hasil Konversi (dalam rupiah) : Rp." + uang3.getHasil());
    }
}
class Uang{
    private double nilai;
    private double hasil;
    private double nilaiDollar = 14350;

    Uang(){
        nilai =1;
        hasil = nilaiDollar;
    }
    Uang(double uang){
        nilai = uang;
        hasil = nilai * nilaiDollar;
    }
    Uang(double uang, double dollar){
        nilai = uang;
        nilaiDollar = dollar;
        hasil =  nilai * nilaiDollar;
    }
    public void setNilai(double nilai) {
        this.nilai = nilai;
    }
    public void setNilaiDollar(double nilaiDollar) {
        this.nilaiDollar = nilaiDollar;
    }
    public double getNilai() {
        return nilai;
    }
    public double getNilaiDollar() {
        return nilaiDollar;
    }

    public double getHasil() {
        return hasil;
    }
}
