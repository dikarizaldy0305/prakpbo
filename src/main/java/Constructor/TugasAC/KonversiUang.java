package Constructor.TugasAC;

import java.util.Scanner;

public class KonversiUang {
    private double nilai;
    private double hasil;
    private double nilaiDollar = 14350;

    KonversiUang(){
        nilai = 1;
        hasil = nilaiDollar;
    }
    KonversiUang(double uang){
        nilai = uang;
        hasil = nilai * nilaiDollar;
    }
    KonversiUang(double uang, double rupiah){
        nilai = uang;
        rupiah = 1;
        hasil =  nilai / rupiah;
    }
    public double getNilai() {
        return nilai;
    }
    public double getHasil() {
        return hasil;
    }

    public static void main(String[] args) {
        double money, money2;
        Scanner scanner = new Scanner(System.in);

        KonversiUang uang = new KonversiUang();
        System.out.println("==== Konversi Uang USD to RP  ====");
        System.out.println("Nilai Dollar saat ini " +uang.getNilai() +" USD = Rp. " + uang.getHasil());

        System.out.println("\n==== Konversi Uang USD ke UERO ===");
        System.out.print  ("Masukkan uang (dalam dollar)   USD ");
        money= scanner.nextDouble();
        KonversiUang uang2 = new KonversiUang(money);
        System.out.println("Hasil konversi (USD Ke Rupiah) Rp. "+ uang2.getHasil());

        System.out.println("\n==== Konversi Uang RP ke USD  ===");
        System.out.print  ("Masukkan nilai (dalam rupiah)  Rp. ");
        money2 = scanner.nextDouble();
        KonversiUang uang3 = new KonversiUang(money, money2 );
        System.out.println("Hasil Konversi (Rupiah ke USD) USD " + uang3.getHasil());
    }
}

