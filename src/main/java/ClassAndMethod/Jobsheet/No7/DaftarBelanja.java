package ClassAndMethod.Jobsheet.No7;

import com.sun.org.apache.xpath.internal.objects.XString;

class barang{
    private String NamaBarang;
    private int HargaBarang;
    public barang (String nama, int harga){
        NamaBarang = nama;
        HargaBarang = harga;
    }

    public String getNamaBarang(){
        return NamaBarang;
    }

    public int getHargaBarang(){
        return HargaBarang;
    }
}

public class DaftarBelanja {
    public static void main(String[] args){
        barang barang1 = new barang("Sapu", 13000);
        barang barang2 = new barang("Spidol", 10000);
        barang barang3 = new barang("Skop", 15000);
        barang barang4 = new barang("Lap Kaca", 20000);

        System.out.println("Daftar Belanja");
        System.out.println("||  Nama Barang  ||  Harga  ");
        System.out.print("||  " + barang1.getNamaBarang()+"\t\t || ");
        System.out.println(barang1.getHargaBarang());
        System.out.print("||  " + barang2.getNamaBarang()+"\t\t || ");
        System.out.println(barang2.getHargaBarang());
        System.out.print("||  " + barang3.getNamaBarang()+"\t\t || ");
        System.out.println(barang3.getHargaBarang());
        System.out.print("||  " + barang4.getNamaBarang()+"\t || ");
        System.out.println(barang4.getHargaBarang());
    }
}
