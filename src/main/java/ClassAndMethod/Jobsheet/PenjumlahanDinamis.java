package ClassAndMethod.Jobsheet;

import java.util.Scanner;

public class PenjumlahanDinamis {
    public static int Tambah (int a, int b){
        return (a + b);
    }

    public static void main (String[] args){
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan nilai a  : ");
        int a = input.nextInt();
        System.out.print("Masukkan nilai b  : ");
        int b = input.nextInt();
        System.out.println("======================");
        System.out.println("Hasil Penjumlahan : "+Tambah(a,b));
    }
}
