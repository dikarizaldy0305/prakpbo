package ClassAndMethod.Jobsheet.No5;

    public class rumusTabung{
        double phi     = 3.14;
        double jarling = 24;
        double tinggi  = 19;

        protected void hitungLuas (){
            double luas = 2 * phi * jarling * (jarling * tinggi);
            System.out.println("Luas persegi panjang adalah "+luas+" cm^2");
        }
        protected void hitungVolume (){
            double volume = phi * jarling * jarling * tinggi;
            System.out.println("Volume tabung adalah "+volume+" cm^3");
        }

    }

