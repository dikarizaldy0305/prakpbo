package ClassAndMethod.Jobsheet.No1;

public class Mahasiswa {
    String nama, nim, alamat;
    public static void main(String[] args){
        Mahasiswa m = new Mahasiswa();
        m.nama = "Andika Rizaldy";
        m.nim  = "F1B019024";
        m.alamat = "Keru, Narmada";
        System.out.println("Nama   : "+ m.nama);
        System.out.println("NIM    : "+ m.nim);
        System.out.println("Alamat : "+ m.alamat);
    }
}
