package ClassAndMethod.Jobsheet.No6_2;

public class RumusLingkaran {
    double phi = 3.14;
    double jarling = 24;

    public void luasLingkaran(){
        double luas = phi * jarling * jarling;
        System.out.println("Luas Lingkaran adalah "+luas+ " cm^2");
    }

    public void kelilingLingkaran(){
        double keliling = 2 * phi * jarling;
        System.out.println("Keliling Lingkaran adalah "+keliling+ " cm^2");
    }
}
