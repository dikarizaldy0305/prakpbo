package ClassAndMethod.Jobsheet.No3;

public class ObjectSecaraStatis {
    String nim, alamat, hobi;
}
class object{
    public static void main(String[] args){
        ObjectSecaraStatis nm1 = new ObjectSecaraStatis();
        ObjectSecaraStatis nm2 = new ObjectSecaraStatis();

        nm1.nim  = "F1B019024";
        nm1.alamat = "Keru, Narmada";
        nm1.hobi = "Belajar Ilmu Baru";
        nm2.nim  = "F1B019005";
        nm2.alamat = "Seruni, Ampenan";
        nm2.hobi = "Tidur";
        System.out.println("NIM    : "+nm1.nim);
        System.out.println("Alamat : "+nm1.alamat);
        System.out.println("Hobi   : "+nm1.hobi);
        System.out.println();
        System.out.println("NIM    : "+nm2.nim);
        System.out.println("Alamat : "+nm2.alamat);
        System.out.println("Hobi   : "+nm2.hobi);
    }
}
