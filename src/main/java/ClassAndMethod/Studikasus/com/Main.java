package ClassAndMethod.Studikasus.com;
import ClassAndMethod.Studikasus.bangunruang.BangunRuang;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        BangunRuang br = new BangunRuang();
        int pilih;
        boolean menu = true;
        do {
            System.out.println("Program Menghitung Volume Bangun Ruang\n" +
                    "1. Kubus " +
                    "2. Tabung " +
                    "3. Limas\n" +
                    "4. Balok " +
                    "5. Bola   " +
                    "0. Exit");
            System.out.print("Masukkan pilihan : ");
            pilih = input.nextInt();
            if (pilih == 1) {
                System.out.println("Menghitung Volume Kubus");
                br.Kubus();
            }else if (pilih == 2){
                System.out.println("Menghitung Volume Tabung");
                br.Tabung();
            }else if (pilih == 3){
                System.out.println("Menghitung Volume Limas");
                br.Limas();
            }else if (pilih == 4){
                System.out.println("Menghitung Volume Balok");
                br.Balok();
            }else if (pilih == 5){
                System.out.println("Menghitung Volume Bola");
                br.Bola();
            }else if (pilih == 0){
                System.out.println("Terima Kasih");
                System.exit(0);
            }else {
                System.out.println("Pilihan Tidak Tersedia, Silahkan Pilih Ulang!");
                System.out.println();
            }
        }while (menu);
    }
}