package ClassAndMethod.Studikasus.bangunruang;
import java.util.Scanner;

public class BangunRuang {
    private double x;
    private double y;
    private double z;
    private double volume;
    double phi = 3.14;
    public void setX(double x) {
        this.x = x;
    }
    public void setY(double y) {
        this.y = y;
    }
    public void setZ(double z) {
        this.z = z;
    }
    public double getX() {
        return x;
    }
    public double getY() {
        return y;
    }
    public double getZ() {
        return z;
    }
    Scanner input = new Scanner(System.in);

    // s * s * s
    public void Kubus() {
        System.out.print("Masukkan sisi : ");
        x = input.nextDouble();
        volume = getX() * getX() * getX();
        System.out.println("Volume kubus adalah " + volume);
        System.out.println();
    }
    // pi * r^2 *t
    public void Tabung(){
        System.out.print("Masukkan  jari jari: ");
        x = input.nextDouble();
        System.out.print("Masukkan  tinggi: ");
        y = input.nextDouble();
        volume = phi * (getX() * getX()) * getY();
        System.out.println("Volume tabung adalah : " + volume);
        System.out.println();
    }
    // 1/3 * a * t
    public void Limas(){
        System.out.print("Masukkan  alas: ");
        x = input.nextDouble();
        System.out.print("Masukkan  tinggi: ");
        y = input.nextDouble();
        volume = 0.3 * getX() * getY();
        System.out.println("Volume limas adalah : " + volume);
        System.out.println();
    }
    //*p * l * t
    public void Balok(){
        System.out.print("Masukkan  panjang: ");
        x = input.nextDouble();
        System.out.print("Masukkan  lebar: ");
        y = input.nextDouble();
        System.out.print("Masukkan  tinggi: ");
        z = input.nextDouble();
        volume =  getX() * getY() * getZ();
        System.out.println("Volume balok adalah : " + volume);
        System.out.println();
    }
    // 4/3 * pi * r ^3
    public void Bola(){
        System.out.print("Masukkan  jari jari: ");
        x = input.nextDouble();
        volume = 1.3 * phi * getX() * getX() * getX();
        System.out.println("Volume bola adalah : " + volume);
        System.out.println();
    }
}
