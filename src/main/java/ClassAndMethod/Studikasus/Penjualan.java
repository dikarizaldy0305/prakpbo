package ClassAndMethod.Studikasus;
import java.util.Scanner;

class Barang{
        String nama;
        int stok;
        int harga;

        void setNama(String nama){
            this.nama = nama;
        }
        void setStok(int stok){
            this.stok= stok;
        }
        void setHarga(int harga){
            this.harga = harga;
        }
        String getNama(){
            return this.nama;
        }
        int getStok(){
            return this.stok;
        }
        int getHarga(){
            return this.harga;
        }

        void display(){
            System.out.println("Nama barang   : " + getNama());
            System.out.println("Stok barang   : " + getStok());
            System.out.println("Harga barang  : " + getHarga());
        }

    }
    class Transaksi{

        int jual(int harga, int uang){
            System.out.println("Harga        : " + harga);
            System.out.println("Uang         : " + uang);
            return harga;
        }
        int hitung(int jmlh, int harga){
            int total =  harga * jmlh;
            return total;
        }
    }

    public class Penjualan {
        public static void main(String[] args) {
            Barang br = new Barang();
            boolean menu = true;
        do {
            Transaksi trf = new Transaksi();
            Scanner input = new Scanner(System.in);
            Barang semen = new Barang();
            Barang paku = new Barang();
            Barang cat = new Barang();
            Barang pipa = new Barang();
            Barang batako = new Barang();


            semen.setNama("Semen");
            paku.setNama("Paku");
            cat.setNama("Cat Tembok");
            pipa.setNama("Pipa");
            batako.setNama("Batako");

            semen.setStok(10);
            paku.setStok(20);
            cat.setStok(50);
            pipa.setStok(55);
            batako.setStok(1000);

            semen.setHarga(10000);
            paku.setHarga(20000);
            cat.setHarga(34000);
            pipa.setHarga(15000);
            batako.setHarga(5000);

            String [] isi = {semen.getNama(), paku.getNama(), cat.getNama(), pipa.getNama(), batako.getNama()};
            System.out.println("==== List Menu ====");
            for (int i = 0; i < isi.length; i++) {
                System.out.println(i+1 + ". " + isi[i]);
            }
            int pilih;
            System.out.print("Masukkan Pilihan     : ");
            pilih = input.nextInt();
            if (pilih == 1) {
                semen.display();
                System.out.print("Masukkan Jumlah      : ");
                int jum = input.nextInt();
                if (jum< semen.getStok()){
                    System.out.println(semen.getStok());
                }else {
                    System.out.println("Barang Tidak Tersedia");
                    semen.display();
                }
                System.out.print("Masukkan Jumlah uang : Rp. ");
                int money = input.nextInt();
                System.out.println("Kembalian    : Rp. " + trf.jual(semen.getHarga(), money));
            } else if(pilih == 2){
                paku.display();
                System.out.print("Masukkan Jumlah       : ");
                int jum = input.nextInt();
                if (jum< semen.getStok()){
                    System.out.println(paku.getStok());
                }else {
                    System.out.println("Barang Tidak Tersedia");
                    paku.display();
                }
                System.out.print("Masukkan Jumlah uang  : Rp. ");
                int money = input.nextInt();
                System.out.println("Kembalian    : Rp. " + trf.jual(paku.getHarga(), money));
            }else if(pilih == 3){
                cat.display();
                System.out.print("Masukkan Jumlah        : ");
                int jum = input.nextInt();
                if (jum< semen.getStok()){
                    System.out.println(cat.getStok());
                }else {
                    System.out.println("Barang Tidak Tersedia");
                    cat.display();
                }
                System.out.print("Masukkan Jumlah uang   : Rp. ");
                int money = input.nextInt();
                System.out.println("Kembalian    : Rp. " + trf.jual(cat.getHarga(), money));
            }else if(pilih == 4){
                pipa.display();
                System.out.print("Masukkan Jumlah         : ");
                int jum = input.nextInt();
                if (jum< semen.getStok()){
                    System.out.println(pipa.getStok());
                }else {
                    System.out.println("Barang Tidak Tersedia");
                    pipa.display();
                }
                System.out.print("Masukkan Jumlah uang    : Rp. ");
                int money = input.nextInt();
                System.out.println("Kembalian    : Rp. " + trf.jual(pipa.getHarga(), money));
            }else if(pilih == 5){
                batako.display();
                System.out.print("Masukkan Jumlah         : ");
                int jum = input.nextInt();
                if (jum< semen.getStok()){
                    System.out.println(batako.getStok());
                }else {
                    System.out.println("Barang Tidak Tersedia");
                    batako.display();
                }
                System.out.println("Total Pembayaran        : "+trf.hitung(jum, batako.harga));
                System.out.print("Masukkan Jumlah uang    : Rp. ");
                int money = input.nextInt();
                System.out.println("Kembalian    : Rp. " + trf.jual(batako.getHarga(), money));
            }else if(pilih == 0){
                System.out.println("Terima Kasih");
                System.exit(0);
            } else{
                System.out.println("Pilihan Tidak Tersedia");
            }
        }while(menu);
        }
}
